# Multi Timer

![Icon](resources/drawables/launcher_icon.png "Icon")

**Quickstart:** After launching the app select your desired timer. Except stopwatches it will take you to the menu. After setting up hit save and you are on main screen.
**Add new:** Use menu at the bottom of the timers list
**Setup/reset:** Long hold up to get to menu. Or use swipe-left gesture.

From times of good old pebble smartwatch I was missing [MultiTimer app](https://developer.rebble.io/developer.pebble.com/community/apps/multi-timer/index.html)  which was super cool and easy. I was sad that there is nothing like that around in garmin family, so I build one.

MultiTimer lets you create several types of timers:
  - Stopwatch - just simple one
  - Countdown timer - set time (like 10 minutes for pasta!)
  - Repeating countdown - when countdown finishes it just starts again automatically
  - Date countdown - give it a date in future (or in past :)) and it will countdown to that date
  - need more? Text me! :-)
  
**Cool features:**
  - timers are running even when the app is closed!
  - you will get a notification about countdown end even when the app is closed
    - NOTE: if your times are ending <5 minutes form each other and you will not open the app when first notification occurs you will get notification after 5 minutes (due to Garmin limitation)
  - you can set notification type like vibration + buzzer or vibration only or none

Note: If you find a bug, please inform me via mail. Also I will be happy for improvements ideas.

## Releases
  - 0.3.6 - added support for fenix8, vivoactive5 and much more
  - 0.3.5 - added support - fēnix® 7 family, Forerunner® 745, Forerunner® 945 LTE, somehow venu® 2 family (need further UI work, sorry)
  - 0.3.4 - Background fire-up fix  
  - 0.3.3 - typos...
  - 0.3.2 - added support (this time for real) - vívoactive® family!, Approach® S62, D2™ Charlie, Descent™ Mk1, fēnix® 6, fēnix® 6 Pro,  fēnix® 6S, fēnix® 6S Pro, Forerunner® 245, Forerunner® 245 Music, Forerunner® 945, MARQ™, somehow VENU (need further UI work) and those star-wars watches
  - 0.3.1 - added support for fenix 6, 6 Pro, 6S, 6S Pro, 6X Pro - not tested please report if something went wrong on those devices - garmin device simulator is not working on new systems (thanks to garmin)
  - 0.3 - a lot of UX improvements (thanks to Wilfrid Chiew and Michael Dugaev for big help! -> issue #3)
  	- when there are no timers it takes you directly to adding a timer (first launch)
	- new timer can be added faster from bottom of the timer list
	- timers can't be changed when they are created
	- save button during editing removed - back button is enough
	- when countdown timer finishes it restart automatically (can be fired up just with one click)
	- there is actual time at the top of the timers list (can be switched off in menu) 
  - 0.2.1 - bugfix - could not remove countdown timers after restart
  - 0.2 - main screen redesign, now it has colors and following some of the garmin UI quidelines, made sound of alarm bit louder and vibration harder
  - 0.1 - first relase already using cool features like app wake ups even when closed and data saving. Four timer types - stopwatch, countdown, repeating countdown, date countdown 


## Future ideas
  - By Ben Rui - Every timer should have possibility to add icon (eg. Burger, pasta, gym, pool, run,...) to distinguish between them (issue #5)
  - By Wilfrid Chiew - Black theme for timer would be cool (issue #4)
  - By Michael Dugaev - Elaborate about battery efficiency, now it draws too much energy...
  - Loop countdown - set up maximum loops for timer (issue #2)
  - More vibration and buzz types 
  
  
## Known bugs
  - make "add new" big like other timers (have to add my own fonts...) 
  - eats battery more than it should...
  - when restarting the app - countdowns are being reordered
  - date and time pickers doesent look like cool timepickers from garmin, they look shitty :(
  - logo is sooo ugly...
  
## Pictures
![screenshot 1](relases/0.3/screenshot_1.png "Screenshot 1")
![screenshot 2](images/2.png "Screenshot 2")
![screenshot 3](images/3.png "Screenshot 3")
![screenshot 4](images/screenshot_2.png "Screenshot 4")
![screenshot 5](images/screenshot_3.png "Screenshot 5")


## APP IDs
 - release version - d66989e3-e128-40e3-b73c-f1027b127d93
 - beta version - 65b7cb65-06e2-44f0-ada8-206938c0e421
