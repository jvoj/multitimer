using Toybox.WatchUi;
using Toybox.System;
using Toybox.Graphics;

class MultiTimerView extends WatchUi.View {

    function initialize() {
        View.initialize();
    }

    // Load your resources here
    function onLayout(dc) {
        setLayout(Rez.Layouts.Timers(dc));
        rowSize = dc.getHeight() / 7.0;
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    	
    }

    // Update the view
    function onUpdate(dc) {
    	
        // Call the parent onUpdate function to redraw the layout
	    dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
	    dc.clear();
	    dc.setColor(Graphics.COLOR_GREEN, Graphics.COLOR_WHITE);
//		dc.drawLine(0, 120, 240, 120);
//		dc.drawLine(120, 0, 120, 240);
		dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_WHITE);
		
		for (var i = 0; i<8;i++){
			drawTimer(timersOnScreen[i], dc, i);	
		}
		if (timers.size() == 1){ // we have only "add new" left...
			timerMenu.newTimer();
		}
		
		
//        View.onUpdate(dc);
    }

	function drawBlackbox(){
		
	}
	
	function drawTimer(timerObject, dc, row) {
		
		var screenWidth = dc.getWidth();
		var screenHeight = dc.getHeight();
		var screenHalf = screenWidth / 2;
		
		// determine if f5 or f6
		var layout;
		if (screenWidth == 454) {
			layout = new Layout454();
		} else if (screenWidth == 416) {
			layout = new Layout416();
		} else if (screenWidth == 240) {
			layout = new LayoutGarmin5();
		} else {
			layout = new LayoutGarmin6();
		}
		
		
		var widthOffset = 77; // from left
		var heightOffset = 4; // from top
		
		if (row == 0 && clockActivated){ // we are on first line and we have clock activated
			// draw grey rectangle
			dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_LT_GRAY);
			dc.fillRectangle(0, rowSize*row, screenWidth, rowSize+6);
			
			var clockTime = System.getClockTime();
			var timeString = clockTime.hour.format("%02d") + ":" +
    						 clockTime.min.format("%02d") + ":" +
    						 clockTime.sec.format("%02d");
			// put there text
			dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
			dc.drawText(
				screenHalf, 
		        heightOffset,
		        Graphics.FONT_MEDIUM,
		        timeString,         
		        Graphics.TEXT_JUSTIFY_CENTER
			);
			return;
		}
		if (row < 3) {
			heightOffset = 0;
		} else if (row > 3){
			heightOffset = 8;
		} 
		
		if (timerObject == null){ // there is no timer to draw
			dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_LT_GRAY);
			if (row == 0){
				dc.fillRectangle(0, rowSize*row, screenWidth, rowSize+6);
			} else if (row == 2){ // up center one
				dc.fillRectangle(0, rowSize*row+6, screenWidth, rowSize-8);
			} else if (row == 3){ // center
				dc.fillRectangle(0, rowSize*row-5, screenWidth, rowSize+20);
			} else if (row == 4) { // down center one
				dc.fillRectangle(0, rowSize*row+13, screenWidth, rowSize+0);
			} else {
				dc.fillRectangle(0, rowSize*row+6, screenWidth, rowSize+0);
			}
			return;
		}
		
		var height;
		var iconHeight;
		var width = widthOffset + 10;
		
		
		var fontSize;
		if (row == 3){ // center one adding time
			height = rowSize*row + heightOffset - 2;
			dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_DK_GRAY);
			
			if (timerObject instanceof AdderTimer){
				fontSize = Graphics.FONT_SYSTEM_SMALL;
				height += 3;
				iconHeight = height + 13;
			} else {
				
				height -= layout.centerHeightOffset;
				iconHeight = height + 15 + layout.centerHeightOffset;
				fontSize = Graphics.FONT_SYSTEM_NUMBER_MEDIUM;
//				System.println(Graphics.getFontHeight(Graphics.FONT_SYSTEM_NUMBER_MEDIUM));
				
				var fontWidth = dc.getTextWidthInPixels(timerObject.getTimeString(), fontSize);
				if (fontWidth + widthOffset > screenWidth) { // if text will not fit :(
					fontSize = Graphics.FONT_NUMBER_MILD; // make it smaller
					height += 6; // and move it bit down
				}
				
				// vivo 55
				// garmin6 74
				// garmin5 36
				
				
			}
			
			
		} else { // adding not center one
			if (timerObject instanceof AdderTimer){
				fontSize = Graphics.FONT_SYSTEM_SMALL;
				height = rowSize*row + heightOffset;
				iconHeight = height + 13;
			} else {
				fontSize = Graphics.FONT_MEDIUM;
				height = rowSize*row + heightOffset;
				iconHeight = height + 13;
			}
			
		}
		
		var supernormalHeight = rowSize*row + rowSize/2;
		// print actual timer
		dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
		dc.drawText(
			width, 
	        supernormalHeight,
	        fontSize,
	        timerObject.getTimeString(),         
	        Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER
		);
		
		var supIconHeight = supernormalHeight - (rowSize/2 - 10) +1 ; // icon size is 20x20
		// state icon
		var runningIcon = WatchUi.loadResource( Rez.Drawables.StopIcon );
		if (timerObject instanceof AdderTimer){
			runningIcon = WatchUi.loadResource( Rez.Drawables.PlusIcon );	
			supIconHeight += 3;	
		} else if (timerObject.running == TimerObject.Running){
			runningIcon = WatchUi.loadResource( Rez.Drawables.StartIcon );
		} else if (timerObject.running == TimerObject.Stopped){
			runningIcon = WatchUi.loadResource( Rez.Drawables.StopIcon );
		} else if (timerObject.running == TimerObject.Paused){
			runningIcon = WatchUi.loadResource( Rez.Drawables.PauseIcon );
		}
		
		dc.drawBitmap(width-25, supIconHeight, runningIcon);
		
		// type icon
		var typeIcon = WatchUi.loadResource( Rez.Drawables.StopwatchIcon );
		if (timerObject instanceof AdderTimer){
			typeIcon = :nope;
		} else if (timerObject instanceof StopwatchTimer){
			typeIcon = WatchUi.loadResource( Rez.Drawables.StopwatchIcon );
		} else if (timerObject instanceof DateCountdownTimer){
			typeIcon = WatchUi.loadResource( Rez.Drawables.DateIcon );
		} else if (timerObject instanceof RepeatingCountdownTimer){
			typeIcon = WatchUi.loadResource( Rez.Drawables.RepeatIcon );
		} else if (timerObject instanceof CountdownTimer){
			typeIcon = WatchUi.loadResource( Rez.Drawables.CountdownIcon );
		}
		
		if (typeIcon != :nope){
			dc.drawBitmap(width-49, supIconHeight, typeIcon);
		}
		
	}

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

}
