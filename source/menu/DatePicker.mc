using Toybox.Application;
using Toybox.Graphics;
using Toybox.System;
using Toybox.WatchUi;
using Toybox.Time;
using Toybox.Time.Gregorian;

const FACTORY_COUNT_24_HOUR = 3;
const FACTORY_COUNT_12_HOUR = 4;
const MINUTE_FORMAT = "%02d";

class DatePicker extends WatchUi.Picker {

    function initialize(defaultTime) {

        var title = new WatchUi.Text({:text=>"Pick date", :locX=>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_BOTTOM, :color=>Graphics.COLOR_WHITE});
        var factories;
        var hourFactory;
        var numberFactories;

        factories = new [11];
        factories[0] = new NumberFactory(0, 23, 1, {});
        factories[1] = new WatchUi.Text({:text=>":", :font=>Graphics.FONT_MEDIUM, :locX =>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_CENTER, :color=>Graphics.COLOR_WHITE});
        factories[2] = new NumberFactory(0, 59, 1, {:format=>MINUTE_FORMAT});
        factories[3] = new WatchUi.Text({:text=>Rez.Strings.timeSeparator, :font=>Graphics.FONT_MEDIUM, :locX =>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_CENTER, :color=>Graphics.COLOR_WHITE});
        factories[4] = new NumberFactory(0, 59, 1, {:format=>MINUTE_FORMAT});
        
        factories[5] = new WatchUi.Text({:text=>",", :font=>Graphics.FONT_MEDIUM, :locX =>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_CENTER, :color=>Graphics.COLOR_WHITE});
        factories[6] = new NumberFactory(0, 31, 1, {:format=>MINUTE_FORMAT});
        factories[7] = new WatchUi.Text({:text=>".", :font=>Graphics.FONT_MEDIUM, :locX =>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_CENTER, :color=>Graphics.COLOR_WHITE});
        factories[8] = new NumberFactory(0, 12, 1, {:format=>MINUTE_FORMAT});
        factories[9] = new WatchUi.Text({:text=>".", :font=>Graphics.FONT_MEDIUM, :locX =>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_CENTER, :color=>Graphics.COLOR_WHITE});
        factories[10] = new NumberFactory(18, 30, 1, {:format=>"%02d"});
        
		
        if(defaultTime != null) {
            defaultTime[0] = factories[0].getIndex(defaultTime[0].toNumber());
            defaultTime[2] = factories[2].getIndex(defaultTime[2].toNumber());
            defaultTime[4] = factories[4].getIndex(defaultTime[4].toNumber());
            
            defaultTime[6] = factories[6].getIndex(defaultTime[6].toNumber());
            defaultTime[8] = factories[8].getIndex(defaultTime[8].toNumber());
            defaultTime[10] = factories[10].getIndex(defaultTime[10].toNumber());
        }

        Picker.initialize({:title=>title, :pattern=>factories, :defaults=>defaultTime});
    }

    function onUpdate(dc) {
        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
        dc.clear();
        Picker.onUpdate(dc);
    }

}

class DatePickerDelegate extends WatchUi.PickerDelegate {

    function initialize() {
        PickerDelegate.initialize();
    }

    function onCancel() {
        WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
        return true;
    }

    function onAccept(values) {
    	var clockTime = System.getClockTime();
        var options = {
        	:second   => values[4],    
        	:minute   => values[2],    
		    :hour     => values[0],    
		    :day      => values[6],
		    :month    => values[8],
		    :year     => values[10]+2000
		};
		
		var settedMoment = Gregorian.moment(options);
		System.println(values);
		System.println(settedMoment.value());
		
//		settedMoment = settedMoment.subtract(new Time.Duration(clockTime.timeZoneOffset));
		// TODO: There is a problem with UTC time... Gregorian.moment thinks I am adding time in UTC format and adds +1 to created time :( Which sucks for computations across time offsets...
		System.println(settedMoment.value());
		
		openedTimer.setValue = settedMoment;
		menu.getItem(menu.findItemById(:timeChange)).setLabel(openedTimer.getSetTimeString());
        WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);

        return true;
    }

}
