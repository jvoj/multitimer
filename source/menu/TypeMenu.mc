using Toybox.Application;
using Toybox.Graphics;
using Toybox.System;
using Toybox.WatchUi;
using Toybox.System;
using Toybox.Time;

class TypeMenu {


	function show(){
		var menu = new WatchUi.Menu2({:title=>"Choose timer type"});
		
		var entry1 = new WatchUi.MenuItem(
                "Countdown",
                "from given duration",
                :Countdown,
                {}
            );
        var entry2 = new WatchUi.MenuItem(
                "Repeating countdown",
                "",
                :RepeatingCountdown,
                {}
            );
        var entry3 = new WatchUi.MenuItem(
                "Date countdown",
                "",
                :DateCountdown,
                {}
            );
        var entry4 = new WatchUi.MenuItem(
                "Stopwatch",
                "",
                :Stopwatch,
                {}
            );
              
		menu.addItem(entry1);
		menu.addItem(entry2);
		menu.addItem(entry3);
		menu.addItem(entry4);
		
		
//			if (openedTimer instanceof CountdownTimer){
//				menu.setFocus(0);
//			}
//	        if (openedTimer instanceof RepeatingCountdownTimer){
//				menu.setFocus(1);
//			}
//			if (openedTimer instanceof DateCountdownTimer){
//				menu.setFocus(2);
//			}
//			if (openedTimer instanceof StopwatchTimer){
//				menu.setFocus(3);
//			}
		
			

        WatchUi.pushView(menu, new TypeMenuDelegate(), WatchUi.SLIDE_IMMEDIATE);
	}
	
	function newType(){
		
	}
		
}

class TypeMenuDelegate extends WatchUi.Menu2InputDelegate {
    function initialize() {
        Menu2InputDelegate.initialize();
    }

    function onSelect(item) {
    
        if (item.getId() == :Countdown){
       		openedTimer = new CountdownTimer(new Time.Duration(60*10));
       		newTimer();
       		return;	
        } else if (item.getId() == :RepeatingCountdown){
       		openedTimer = new RepeatingCountdownTimer(new Time.Duration(60*10));
       		newTimer();
       		return;	
        } else if (item.getId() == :DateCountdown){
       		openedTimer = new DateCountdownTimer(Time.now());
       		newTimer();
       		return;	
        } else if (item.getId() == :Stopwatch){
        	openedTimer = new StopwatchTimer(0);
        	timerMenu.saveNew();
        	
        	return;	
        }
        WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
    }
    
    function onBack() {
    	if (timers.size() == 1) { // there is no timer, so back button should exit the app & and save state!
    		persistanceService.persistTimers();
    		backgwoundWakeService.registerWake();
    		WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
    		WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
    	} else {
    		WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
    	}
    }
    
    function newTimer(){
//        WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
//        WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
		timerMenu.createTimerMenu();
    }
}

 