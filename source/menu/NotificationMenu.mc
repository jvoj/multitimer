using Toybox.WatchUi;
using Toybox.System;
using Toybox.Time;


class NotificationMenu {

	
	
	function show(){
		var notifMenu = new WatchUi.Menu2({:title=>"Notification type"});
        
        notifMenu.addItem(
            new WatchUi.MenuItem(
                "Buzz + vibe",
                null,
                :BuzzVibrations,
                {}
            )
        );
        
        notifMenu.addItem(
            new WatchUi.MenuItem(
                "Short vibrations",
                null,
                :vibrations,
                {}
            )
        );        
        notifMenu.addItem(
            new WatchUi.MenuItem(
                "None",
                null,
                :none,
                {}
            )
        );
        
		if (openedTimer.notificationType == TimerObject.BuzzVibration){
			notifMenu.setFocus(0);
		} else if (openedTimer.notificationType == TimerObject.Vibration){
			notifMenu.setFocus(1);
		} else if (openedTimer.notificationType == TimerObject.Nothing){
			notifMenu.setFocus(2);
		}
        
        WatchUi.pushView(notifMenu, new NotificationMenuDelegate(), WatchUi.SLIDE_IMMEDIATE);
	}
}

class NotificationMenuDelegate extends WatchUi.Menu2InputDelegate {
    function initialize() {
        Menu2InputDelegate.initialize();
    }

    function onSelect(item) {
    	if (item.getId() == :BuzzVibrations){
    		openedTimer.notificationType = TimerObject.BuzzVibration;
    	} else if (item.getId() == :vibrations){
    		openedTimer.notificationType = TimerObject.Vibration;
    	} else if (item.getId() == :none){
    		openedTimer.notificationType = TimerObject.Nothing;
    	}
    	menu.getItem(menu.findItemById(:notificationChange)).setLabel(openedTimer.getNotificationTypeString());
        WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
    }
}