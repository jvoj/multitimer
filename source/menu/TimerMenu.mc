using Toybox.WatchUi;
using Toybox.System;
using Toybox.Time;


class TimerMenu {

	function initialize(){
	
	}

	function editTimer(){
		openedTimerNew = false;
		createTimerMenu();
	}
	
	function newTimer(){
		if (!openedTimerEditing || timers.size() > 1){ // when we have no timers yet - be careful about opening two in row...
//			openedTimer = new StopwatchTimer(0);
			openedTimerNew = true;
			openedTimerEditing = true;
			TypeMenu.show();
			
		}
	}
	
	function saveNew(){
		openedTimerEditing = false;
       	if (timers.size() > 0){
        	timers = timers.slice(0, -1); // slice last element (which is adder timer)
       	}
       	timers.add(openedTimer); // add this new timer
	    timers.add(new AdderTimer());
//     	pointerOnScreen = timers.size()-1;
        recalculateTimerPositions();
        WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
        
	}
	
	function createTimerMenu(){
		var onSave, onRemove;
		if (openedTimerNew){
			onSave = :saveNew;
			onRemove = :removeNew;
		} else {
			onSave = :save;
			onRemove = :remove;
		}
		if (!openedTimerNew){
			menu = new WatchUi.Menu2({:title=>openedTimer.getName()});
		} else {
			menu = new WatchUi.Menu2({:title=>"Adding " + openedTimer.getName()});
		}
        var delegate;

//		if (openedTimerNew){
//			menu.addItem(
//	            new WatchUi.MenuItem(
//	                openedTimer.getName(),
//	                openedTimer.getAbout(),
//	                :changeType,
//	                {}
//	            )
//	        );
//        }
        if (!(openedTimer instanceof StopwatchTimer)){
	        menu.addItem(
	            new WatchUi.MenuItem(
	                openedTimer.getSetTimeString(),
	                null,
	                :timeChange,
	                {}
	            )
	        );
	        menu.addItem(
	            new WatchUi.MenuItem(
	                openedTimer.getNotificationTypeString(),
	                "Notification type",
	                :notificationChange,
	                {}
	            )
	        );

	        menu.addItem(
	            new WatchUi.ToggleMenuItem(
	                "Notify when closed",
	                "",
	                :notificationWhenClosed,
	                openedTimer.notificationWhenClosed,
	                {}
	            )
	        );
        }
		if (!openedTimerNew && !(openedTimer instanceof DateCountdownTimer)){
			menu.addItem(
	            new WatchUi.MenuItem(
	                "Reset",
	                null,
	                :resetTimer,
	                {}
	            )
	        );
        }
        
        if (openedTimerNew){
	        menu.addItem(
	            new WatchUi.MenuItem(
	                "Save",
	                null,
	                :saveNew,
	                {}
	            )
	        );        
        }
        if (timers.size() > 0 && !openedTimerNew){
	        menu.addItem(
	            new WatchUi.MenuItem(
	                "Remove",
	                null,
	                onRemove,
	                {}
	            )
	        );
        }
        
        menu.addItem(
	            new WatchUi.ToggleMenuItem(
	                "Clock on top",
	                "",
	                :toggleClockOnTop,
	                clockActivated,
	                {}
	            )
	        );
//        if (timers.size() > 0 && openedTimerNew){
//        	menu.addItem(
//	            new WatchUi.MenuItem(
//	                "Cancel adding",
//	                null,
//	                onRemove,
//	                {}
//	            )
//	        );
//        }
//        if (!openedTimerNew){
//			menu.addItem(
//	            new WatchUi.MenuItem(
//	                "Add new",
//	                null,
//	                :newTimer,
//	                {}
//	            )
//	        );
//        }
		
		if (openedTimer.running == TimerObject.Paused){ // if we are paused put focus on reset
			menu.setFocus(menu.findItemById(:resetTimer));
		}

        WatchUi.pushView(menu, new TimerMenuDelegate(), WatchUi.SLIDE_IMMEDIATE);


	}

}