using Toybox.Application;
using Toybox.Graphics;
using Toybox.System;
using Toybox.WatchUi;
using Toybox.Time;
using Toybox.Time.Gregorian;

class TimerMenuDelegate extends WatchUi.Menu2InputDelegate {
    function initialize() {
        Menu2InputDelegate.initialize();
    }

    function onSelect(item) {
        if (item.getId() == :changeType){
        	TypeMenu.show();
        } else if (item.getId() == :timeChange){
        	if (openedTimer instanceof CountdownTimer || openedTimer instanceof RepeatingCountdownTimer){ 
        		var hours = openedTimer.setValue.value() / 3600;
			    var minutes = (openedTimer.setValue.value() /60) % 60;
			    var seconds = openedTimer.setValue.value() % 60;
        		
        		var defaultValues = [hours, 0, minutes, 0, seconds];
        		WatchUi.pushView(new CountdownPicker(defaultValues), new CountdownPickerDelegate(), WatchUi.SLIDE_IMMEDIATE);
        	
        	} else if (openedTimer instanceof DateCountdownTimer){
        		var clockTime = System.getClockTime();
				var offsetDuration = new Time.Duration(clockTime.timeZoneOffset);
				var dateFormatted = Gregorian.info(openedTimer.setValue.subtract(offsetDuration), Time.FORMAT_SHORT);
//        		var dateFormatted = Gregorian.info(openedTimer.setValue, Time.FORMAT_SHORT);
        		var defaultValues = [
        						dateFormatted.hour, 0,
        						dateFormatted.min,  0,
        						dateFormatted.sec, 0,
        						dateFormatted.day, 0,
        						dateFormatted.month, 0,
        						dateFormatted.year-2000];
        						
        		if (openedTimerNew){
        			defaultValues[2] = 0;
        			defaultValues[4] = 0;
        		}
				WatchUi.pushView(new DatePicker(defaultValues), new DatePickerDelegate(), WatchUi.SLIDE_IMMEDIATE);        						
        						
        	
        	}
        } else if (item.getId() == :notificationChange){	
        	NotificationMenu.show();
        } else if (item.getId() == :notificationWhenClosed){	
        	openedTimer.notificationWhenClosed = !openedTimer.notificationWhenClosed;
        	menu.getItem(menu.findItemById(:notificationWhenClosed)).setEnabled(openedTimer.notificationWhenClosed);
        	
        } else if (item.getId() == :resetTimer){	
        	timers[pointerOnScreen].stop();
        	WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);     	
        } else if (item.getId() == :saveNew){
        	timerMenu.saveNew(); // has to be outside because TypeMenu also uses that on "rapid adding" stopwatch
        	WatchUi.popView(WatchUi.SLIDE_IMMEDIATE); // we need to go throught type menu also
        } else if (item.getId() == :remove){
        	openedTimerEditing = false;
        	timers.remove(openedTimer);
        	pointerOnScreen = 0;
        	recalculateTimerPositions();
        	WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
        } else if (item.getId() == :removeNew){
        	openedTimerEditing = false;
        	WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
        	WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
        	if (timers.size() == 1) { // when we have just one "adder timer" left canceling should stop the app
        		System.println("Slide it all out");	
        		WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
        	}
        } else if (item.getId() == :newTimer){
			timerMenu.newTimer();
        } else if (item.getId() == :toggleClockOnTop){
        	clockActivated = !clockActivated;
        }
    }
    
    function onBack(){
    	if (!openedTimerNew){
    		// save it! (update existing)
    		openedTimerEditing = false;
	       	timers[pointerOnScreen] = openedTimer;
	       	recalculateTimerPositions();
    	}
    	
    	WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
    	
    }
}

 