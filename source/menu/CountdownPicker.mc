using Toybox.Application;
using Toybox.Graphics;
using Toybox.System;
using Toybox.WatchUi;
using Toybox.Time;

class CountdownPicker extends WatchUi.Picker {

    function initialize(defaultTime) {

        var title = new WatchUi.Text({:text=>"Pick duration", :locX=>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_BOTTOM, :color=>Graphics.COLOR_WHITE});
        var factories;
        var hourFactory;
        var numberFactories;

        factories = new [5];
        factories[0] = new NumberFactory(0, 23, 1, {});
        factories[1] = new WatchUi.Text({:text=>":", :font=>Graphics.FONT_MEDIUM, :locX =>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_CENTER, :color=>Graphics.COLOR_WHITE});
        factories[2] = new NumberFactory(0, 59, 1, {:format=>MINUTE_FORMAT});
        factories[3] = new WatchUi.Text({:text=>Rez.Strings.timeSeparator, :font=>Graphics.FONT_MEDIUM, :locX =>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_CENTER, :color=>Graphics.COLOR_WHITE});
        factories[4] = new NumberFactory(0, 59, 1, {:format=>MINUTE_FORMAT});
		
        if(defaultTime != null) {
            defaultTime[0] = factories[0].getIndex(defaultTime[0].toNumber());
            defaultTime[2] = factories[2].getIndex(defaultTime[2].toNumber());
            defaultTime[4] = factories[4].getIndex(defaultTime[4].toNumber());
        }

        Picker.initialize({:title=>title, :pattern=>factories, :defaults=>defaultTime});
    }

    function onUpdate(dc) {
        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
        dc.clear();
        Picker.onUpdate(dc);
    }

}

class CountdownPickerDelegate extends WatchUi.PickerDelegate {

    function initialize() {
        PickerDelegate.initialize();
    }

    function onCancel() {
        WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
        return true;
    }

    function onAccept(values) {
        System.println(values);
        var seconds = values[0]*60*60 + values[2]*60 + values[4];
		openedTimer.setValue = new Time.Duration(seconds);
//		menu.getItem(1).setLabel(TimerObject.secondsToTimeString(seconds));
		menu.getItem(menu.findItemById(:timeChange)).setLabel(TimerObject.secondsToTimeString(seconds));
        WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
        return true;
    }

}
