using Toybox.Application;
using Toybox.WatchUi;
using Toybox.Graphics as Gfx;
using Toybox.Timer;
using Toybox.Time;
using Toybox.Time.Gregorian;
 
//var timers = [new StopwatchTimer(0), new CountdownTimer(new Time.Duration(10))];
var timers;

var timersOnScreen; // always 8


//var timersOnScreen = ["","","","","","",""];  always 7

var pointerOnScreen; 
var openedTimer;
var openedTimerEditing;
var openedTimerNew;
var menu;
var timerMenu;
var secTimer;
var persistanceService;
var backgwoundWakeService;
var clockActivated;
var rowSize; // needed for touch screens and drawing


function recalculateTimerPositions(){
		for (var i = 0; i < timersOnScreen.size();i++){
			
			var realPos = ((i) % timersOnScreen.size());
			var timersPos = pointerOnScreen+i - 3; 
			if (timersPos < timers.size() && timersPos >= 0){
				timersOnScreen[realPos] = timers[timersPos];
			} else {
				timersOnScreen[realPos] = null; // just put there dummy
			}
		}
		WatchUi.requestUpdate();
	}


class MultiTimerApp extends Application.AppBase {
	
    function initialize() {
        AppBase.initialize();
        timers = [];
        timersOnScreen = [null, null, null, null, null, null, null, null];
    }
    
    // This method is called when data is returned from our
    // Background process.
    function onBackgroundData(data) {
        
    }

    // onStart() is called on application start up
    function onStart(state) {
    	
    }

    // onStop() is called when your application is exiting
    function onStop(state) {
    	
    }

    // Return the initial view of your application here
    function getInitialView() {
    	pointerOnScreen = 0;
    	openedTimerEditing = false;
    	timerMenu = new TimerMenu();
        secTimer = new SecondTimer();
        persistanceService = new PersistanceService();
        persistanceService.restoreSavedTimers();
        backgwoundWakeService = new BackgroundWakeService();
        backgwoundWakeService.clearWakes();
//        timers.add(new StopwatchTimer(0));
//        timers.add(new StopwatchTimer(0));
//        timers.add(new AdderTimer());
        recalculateTimerPositions();
        return [ new MultiTimerView(), new MultiTimerDelegate() ];
    }
    
//     This method runs each time the background process starts.
    function getServiceDelegate(){
        return [new BackgroundWakeDelegate()];
    }

}
