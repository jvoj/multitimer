using Toybox.Background as BG;
using Toybox.System;
using Toybox.Time;

(:background)
function determineClosestWake(timerArray){
	var closestWake = null;
	var now = new Time.Moment(Time.now().value());
	
	for (var i = 0; i < timerArray.size();i++){
		var timerEnd = timerArray[i];
		if (timerEnd != null){
			if (closestWake == null){ // so far nothing assigned, assign
				closestWake = timerEnd; 
			} else {
				if (closestWake > timerEnd && timerEnd > now.value()){ // is timer time bigger than already found thingy?
					closestWake = timerEnd;
				}
			}
		}
	}
	return closestWake;
}

(:background)
class BackgroundWakeDelegate extends System.ServiceDelegate {	
	function initialize(){
		System.ServiceDelegate.initialize();
		System.println("Background task fired up...");
	}

    // If our timer expires, it means the application timer ran out,
    // and the main application is not open. Prompt the user to let them
    // know the timer expired.
    function onTemporalEvent() {
    	
    	var wakeupTimes = Application.Storage.getValue("wakeupTimes");
    	if (wakeupTimes != null) { // if we have timer to schedule
	    	var closestWake = determineClosestWake(wakeupTimes);
	    	
	    	if (closestWake == null){
	    		tellNormal();
	    	} else {
	    		var lastTime = BG.getLastTemporalEventTime();
				var nextTime = lastTime.add(new Time.Duration(5 * 60));
		    	
		    	if (nextTime.value() <= closestWake){ // if we are in 5 min interval
		    		BG.registerForTemporalEvent(new Time.Moment(closestWake));
		    		tellNormal();
		    		System.println("So we will wake up at " + closestWake);
		    	} else {
		    		BG.registerForTemporalEvent(nextTime); // just to it after 5 min... sucks but...
		    		System.println("So we will wake up at " + nextTime.value());
		    		tellWithWarning();
		    	}
	    	}
		} else {
			tellNormal();
		}
	        	
        BG.exit(true);
    }
    
    function tellNormal(){
    	BG.requestApplicationWake("One of your MultiTimers is about to end!");
    }
    
    function tellWithWarning(){
    	BG.requestApplicationWake("One of your MultiTimers is about to end! You have to open app to not loose next notifications.");
    }
}

class BackgroundWakeService {

	function initialize(){
//		System.ServiceDelegate.initialize();
	}
    

	function registerWake(){ // called on app end
		var wakeupTimes = [];
		for (var i = 0;i<timers.size();i++){
			var timerEnd = timers[i].endWillOccurAt();
			if (timerEnd != null){
				wakeupTimes.add(timerEnd.value());
			}
		}
		
		
		var closestWake = determineClosestWake(wakeupTimes);
//		System.println(wakeupTimes + " " + closestWake);
		wakeupTimes.remove(closestWake);
		Application.Storage.setValue("wakeupTimes", wakeupTimes);
		
		
		if (closestWake != null){
			System.println("So we will wake up at " + closestWake);
		} else {
			System.println("We will not wake up");
		}
		
		if (closestWake != null){
			Background.registerForTemporalEvent(new Time.Moment(closestWake));
		}
	}
	
	function clearWakes(){ // called on app start
		System.println("Clearing wakes");
		Background.deleteTemporalEvent(); // delete all temporal events
		Application.Storage.setValue("wakeupTimes", null);
	}

} 