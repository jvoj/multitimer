using Toybox.Application;
using Toybox.WatchUi;
using Toybox.Timer;
using Toybox.Time;


class PersistanceService {

	
	function initialize(){
				
	}


	function persistTimers(){
		Application.Storage.clearValues();
		var countdowns = [];
		var stopwatches = [];
		var dateCountdowns = [];
		var repeatingCountdowns = [];
	
		for (var i=0; i < timers.size();i++){
			timers[i].convertForPersist();
			
			if (timers[i] instanceof RepeatingCountdownTimer){
				repeatingCountdowns.add(timers[i].toPersist);
			} else if (timers[i] instanceof StopwatchTimer){
				stopwatches.add(timers[i].toPersist);
			} else if (timers[i] instanceof DateCountdownTimer){
				dateCountdowns.add(timers[i].toPersist);
			} else if (timers[i] instanceof CountdownTimer){
				countdowns.add(timers[i].toPersist);
			} else {
//				System.println("Unknown timer will not be saved...");
			}
		}
		
		if (countdowns.size() > 0){
			Application.Storage.setValue("countdowns", countdowns);	
		}
		
		if (stopwatches.size() > 0){
			Application.Storage.setValue("stopwatches", stopwatches);
		}

		if (dateCountdowns.size() > 0){
			Application.Storage.setValue("dateCountdowns", dateCountdowns);
		}
		
		if (repeatingCountdowns.size() > 0){
			Application.Storage.setValue("repeatingCountdowns", repeatingCountdowns);
		}
		
		Application.Storage.setValue("clockActivated", clockActivated);
	}	
	
	function restoreSavedTimers(){
		var countdowns = Application.Storage.getValue("countdowns");
		var stopwatches = Application.Storage.getValue("stopwatches");
		var dateCountdowns = Application.Storage.getValue("dateCountdowns");
		var repeatingCountdowns = Application.Storage.getValue("repeatingCountdowns");
		clockActivated = Application.Storage.getValue("clockActivated");
		if (clockActivated == null){ // if there is no entry yet
			clockActivated = true;
		}
		
		var gotTimers = [];
		
		if (countdowns != null){
			for (var i=0;i<countdowns.size();i++){
				var newTimer = new CountdownTimer(new Time.Duration(10));
				newTimer.convertFromPersist(countdowns[i].reverse());
				gotTimers.add(newTimer);
			}
		}
		
		if (stopwatches != null){
			for (var i=0;i<stopwatches.size();i++){
				var newTimer = new StopwatchTimer(0);
				newTimer.convertFromPersist(stopwatches[i].reverse());
				gotTimers.add(newTimer);
			}
		}
		
		if (dateCountdowns != null){
			for (var i=0;i<dateCountdowns.size();i++){
				var newTimer = new DateCountdownTimer(Time.now());
				newTimer.convertFromPersist(dateCountdowns[i].reverse());
				gotTimers.add(newTimer);
			}
		}
		
		if (repeatingCountdowns != null){
			for (var i=0;i<repeatingCountdowns.size();i++){
				var newTimer = new RepeatingCountdownTimer(new Time.Duration(60*10));
				newTimer.convertFromPersist(repeatingCountdowns[i].reverse());
				gotTimers.add(newTimer);
			}
		}
		
		
		
		gotTimers.add(new AdderTimer());
		timers = gotTimers;
		
						
	}


}