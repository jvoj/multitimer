using Toybox.WatchUi;
using Toybox.System;
using Toybox.Time;

class MultiTimerDelegate extends WatchUi.BehaviorDelegate {

	function initialize() {
		WatchUi.BehaviorDelegate.initialize();
	}

 	function onMenu(){
    	longUp();
    	return true;
    }
    
    
    function onKey(keyEvent){
    	switch (keyEvent.getKey()){
    		case WatchUi.KEY_UP: up(); break;
    		case WatchUi.KEY_DOWN: down(); break;
    		case WatchUi.KEY_ENTER: select(); break;
    		case WatchUi.KEY_ESC: closeApp(); break; // exit the app
    	}
//    	System.println("Press " + keyEvent.getKey());
    	return true;
    }
    
    function onSwipe(swipeEvent) { // touchscreen
    	System.println("swipe " + swipeEvent.getDirection());
    	switch (swipeEvent.getDirection()){
    		case WatchUi.SWIPE_UP: down(); break;
    		case WatchUi.SWIPE_RIGHT: closeApp();  break;
    		case WatchUi.SWIPE_DOWN: up(); break;
    		case WatchUi.SWIPE_LEFT: longUp(); break;
    	}
    	return true;
    }
    
    function onTap(clickEvent) { // touchscreen
    	System.println("tap " + clickEvent.getCoordinates() + " " + clickEvent.getType());
    	var y = clickEvent.getCoordinates()[1];
    	var whichLine = y / rowSize;
    	
    	System.println("tap " + clickEvent.getCoordinates() + " " + clickEvent.getType() + " " + rowSize.format("%.2f") + ", " + whichLine.format("%.2f"));
    	
    	if (whichLine <= 3) { // clicked top
    		up();
    	} else if (whichLine <= 4) { // clicked center
    		if (clickEvent.getType() == WatchUi.CLICK_TYPE_HOLD) {
    			select();
    		} else {
    			select();
    		}
    	} else { // cliecked bottom
    		down();
    	}
    	
    	return true;
    }

    function down(){
    	if (pointerOnScreen == timers.size()-1){
       		pointerOnScreen = 0;
       	} else {
       		pointerOnScreen++;
       	}
       	
    	recalculateTimerPositions();
       	
    }
    
    function longDown(){
    	down();
    }
    
    function up(){
    	if (pointerOnScreen == 0){
        	pointerOnScreen = timers.size();
        }
        pointerOnScreen--;
        
    	recalculateTimerPositions();
    }
    
    function longUp(){
		if (timers.size() > 0){
			if (!(timers[pointerOnScreen] instanceof AdderTimer)){
				openedTimer = timers[pointerOnScreen];
				timerMenu.editTimer();
			} else {
				timers[pointerOnScreen].toggle(); // this basiclally creates new menu for adding (just to be on one place)
			}
		} else {
			timerMenu.newTimer();
		}
    	
    }
    
    function select(){
    	if (timers.size() > 0){
    		timers[pointerOnScreen].toggle();
    	} else {
    		timerMenu.newTimer();
    	}
    }
    
    function longSelect(){
    	timers[pointerOnScreen].stop();
    }
    
    
    function closeApp(){
	    System.println("exiting the app");
		persistanceService.persistTimers();
		backgwoundWakeService.registerWake();
		WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
    }
    
    
    
   
    
    
}