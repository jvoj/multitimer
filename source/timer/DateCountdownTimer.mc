using Toybox.Time;
using Toybox.System;
using Toybox.Time.Gregorian;


class DateCountdownTimer extends TimerObject {
	
	function initialize(aTime){
		TimerObject.initialize(aTime);
	}
	
	function reset(){
		// cant reset 
	}
	
	function start(){
    	TimerObject.start();
	}
	
	function stop(){
		TimerObject.stop();
	}
	
	function pause(){
		// cant pause
	}
	
	function toggle(){
		// cant toggle		
	}
	
	function getTimeString(){
		var now = new Time.Moment(Time.now().value());
		var clockTime = System.getClockTime();
		var offsetDuration = new Time.Duration(clockTime.timeZoneOffset);
		var remainingSeconds = setValue.subtract(offsetDuration).compare(now);
//		var remainingSeconds = setValue.compare(now);
		if (remainingSeconds == 0){ // stop if we have done it
			end();
		}
		var finalString = "";
		if (remainingSeconds < 0){
			finalString += "-";
		}
			
		finalString += TimerObject.secondsToTimeString(remainingSeconds);
		return finalString;
	}
	
	function end(){
		TimerObject.doNotification();
	}
	
	function getSetTimeString(){
		var clockTime = System.getClockTime();
		var offsetDuration = new Time.Duration(clockTime.timeZoneOffset);
		var toDateString = Gregorian.info(setValue.subtract(offsetDuration), Time.FORMAT_SHORT);
//		var toDateString = Gregorian.utcInfo(setValue, Time.FORMAT_SHORT);
		var dateString = toDateString.hour.format("%02d") + ":" 
					   + toDateString.min.format("%02d") + ":"
					   + toDateString.sec.format("%02d") + " "
					   + toDateString.day + "."
					   + toDateString.month + "."
					   + toDateString.year;
			
		return dateString;	
	}
	
	function endWillOccurAt(){
		if (!notificationWhenClosed){
			return null;
		}
		var now = new Time.Moment(Time.now().value());
		var remainingSeconds = setValue.compare(now);
		if (remainingSeconds > 0){
			return setValue;
		} else {
			return null;
		}
	}
	
	function convertForPersist(){
		toPersist = [];
		toPersist.add(setValue.value());
		TimerObject.convertForPersist();
	}

	function convertFromPersist(persistedContent){
		var persitedSetValue = persistedContent.slice(-1, null);
		setValue = new Time.Moment(persitedSetValue[0]);
		persistedContent.remove(persitedSetValue[0]);
		
		TimerObject.convertFromPersist(persistedContent);		
	}

	
	function getName(){
		return "Date countdown";
	}
	
	function getAbout(){
		return "";
	}
	
	
}