using Toybox.Time;
using Toybox.System;
using Toybox.Time.Gregorian;


class AdderTimer extends TimerObject {
	
	
	function initialize(){
		
	}
	
	function reset(){
	
	}
	
	function start(){
	
	}
	
	function stop(){
	
	}
	
	function toggle(){
		timerMenu.newTimer();
	}
	
	function pause(){
		
	}

	function updateActual(){
		
	}
	

	
	function getTimeString(){
		return "add new";
	}
	
	function getSetTimeString(){
		
	}
	
	function endWillOccurAt(){
		return null;
	}
	
	function convertForPersist(){
		
	}

	function convertFromPersist(persistedContent){
		
	}
	
	function getName(){
		return "Dummy adder";
	}
	
	function getAbout(){
		return "";
	}
	
	
}