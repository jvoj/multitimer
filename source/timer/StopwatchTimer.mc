using Toybox.Time;
using Toybox.System;
using Toybox.Time.Gregorian;


class StopwatchTimer extends TimerObject {
	
	var lastUpdateTime;
	
	function initialize(aTime){
		TimerObject.initialize(aTime);
		stop();
	}
	
	function reset(){
		actualValue = 0;
		lastUpdateTime = 0;
	}
	
	function start(){
		lastUpdateTime = new Time.Moment(Time.now().value());
		TimerObject.start();
	}
	
	function stop(){
		reset();
		TimerObject.stop();

	}
	
	function toggle(){
		updateActual();
		TimerObject.toggle();
	}
	
	function pause(){
		TimerObject.pause();
	}

	function updateActual(){
		if (running == TimerObject.Running){
			var now = new Time.Moment(Time.now().value());
			actualValue += now.compare(lastUpdateTime);
			lastUpdateTime = now;
		} else if (running == TimerObject.Stopped){
		
		} else if (running == TimerObject.Paused){
		
		}
	}
	

	
	function getTimeString(){
		updateActual();	
		if (actualValue == 0){ // just for debug
			return "00:00";
		} else {
			if (running == TimerObject.Running){
				return secondsToTimeString(actualValue);
			} else if (running == TimerObject.Paused) {
				return secondsToTimeString(actualValue);
			} else if (running == TimerObject.Stopped) {
				return "00:00";
			} else {
				return "ERROROS";
			}
		}
	}
	
	function getSetTimeString(){
		if (actualValue == 0){ // just for debug
			return "---";
		} else {
			return "timestring";
		}
	}
	
	function endWillOccurAt(){
		return null;
	}
	
	function convertForPersist(){
		toPersist = [];
		if (lastUpdateTime instanceof Time.Moment){
			toPersist.add(lastUpdateTime.value());
		} else {
			toPersist.add(0);
		}
		
		toPersist.add(actualValue);
		
		TimerObject.convertForPersist();
	}

	function convertFromPersist(persistedContent){
		var persitedStartTime = persistedContent.slice(-1, null);
		lastUpdateTime = new Time.Moment(persitedStartTime[0]);
		persistedContent.remove(persitedStartTime[0]);
		
		var persitedActualValue = persistedContent.slice(-1, null);
		actualValue = persitedActualValue[0];
		persistedContent.remove(persitedActualValue[0]);
				
				
		TimerObject.convertFromPersist(persistedContent);
	}
	
	function getName(){
		return "Stopwatch";
	}
	
	function getAbout(){
		return "";
	}
	
	
}