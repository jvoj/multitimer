using Toybox.Time;
using Toybox.System;
using Toybox.Time.Gregorian;
using Toybox.Attention;
//using Toybox.Lang.Number;

/*
	Countdown, RepeatingCountdown - got time in seconds
		
	DateCountdown - got date,
	Stopwatch - nothing just adding seconds and showing result
*/


class TimerObject {
	enum {
		// states
		Stopped,
		Running,
		Paused,
		
		// notification types
		BuzzVibration,
		Vibration,
		Nothing,
	}
	
	var setValue;
	var actualValue;
	var running;
	var notificationType;
	var notificationWhenClosed;
	var toPersist;
	var order;
	
	function start(){
		running = Running;
	}
	
	function stop(){
		running = Stopped;
	}
	
	function pause(){
		running = Paused;
	}
	
	function reset(){
	
	}
	
	function toggle(){
		if (running == Running){
			pause();
		} else if (running == Paused) {
			start();
		} else if (running == Stopped){
			reset();
			start();
		} else {
			// well? xD
		}
		
		WatchUi.requestUpdate();
	}
	
	function initialize(aTime){	
		setValue = aTime;
		actualValue = null;
		running = Running; // TODO: FIX to Stopped
		notificationType = Vibration;
		notificationWhenClosed = true;
	}
	
	public function secondsToTimeString(totalSeconds) {
		totalSeconds = totalSeconds.abs(); // abs
		
		var days = totalSeconds / (3600*24);
		totalSeconds -= days * 3600*24;
	    var hours = totalSeconds / 3600;
	    var minutes = (totalSeconds /60) % 60;
	    var seconds = totalSeconds % 60;
	    if (days > 100) {
	    	return format("$1$.", [days.format("%01d")]);
	    } else if (days < 100 && days > 0  && hours >= 0 && minutes >= 0 && seconds >= 0){
	    	return format("$1$. $2$:$3$", [days.format("%01d"), hours.format("%01d"), minutes.format("%02d")]);
	    } if (hours > 0 && minutes >= 0 && seconds >= 0){
	    	return format("$1$:$2$:$3$", [hours.format("%01d"), minutes.format("%02d"), seconds.format("%02d")]);
	    } else if (minutes >= 0 && seconds >= 0){
	    	return format("$1$:$2$", [minutes.format("%02d"), seconds.format("%02d")]);
	    } else {
	    	return "00:00";
	    }
	}
	
	function getNotificationTypeString(){
		if (notificationType == BuzzVibration){
			return "Buzz + vibration";
		} else if (notificationType == Vibration){
			return "Short vibration";
		} else if (notificationType == Nothing){
			return "Nothing";
		} else {
			return "dunnno";
		}
	}
	
	
	function doNotification(){
		var vibeData = [];
		if (Attention has :vibrate){
		vibeData = [
	        new Attention.VibeProfile(100, 500), // On for two seconds
	    ];
    }
	
		if (notificationType == BuzzVibration){
			if (Attention has :vibrate){
				Attention.vibrate(vibeData);
			}
			
			if (Attention has :playTone) {
				Attention.playTone(Attention.TONE_ALERT_HI);
			}
			
		} else if (notificationType == Vibration){
			if (Attention has :vibrate){
				Attention.vibrate(vibeData);
			}
		} else if (notificationType == Nothing){
			// ?
		} else {
			// ?
		}
	}
	
	function convertForPersist(){

		if (running == Stopped){
			toPersist.add(0);				 
		} else if (running == Running){
			toPersist.add(1); 
		} else if (running == Paused){
			toPersist.add(2);
		} else {
			toPersist.add(3);
		}		


		if (notificationType == BuzzVibration){
			toPersist.add(0);				 
		} else if (notificationType == Vibration){
			toPersist.add(1); 
		} else if (notificationType == Nothing){
			toPersist.add(2);
		} else {
			toPersist.add(3);
		}
		
		if (notificationWhenClosed){
			toPersist.add(1);				 
		} else {
			toPersist.add(0);
		}
		
		
	}
	
	function convertFromPersist(persistedContent){
		var pNotificationWhenClosed = persistedContent[0];
		var pNotificationType = persistedContent[1];
		var pRunning = persistedContent[2];
		
		
		switch (pRunning){
			case 0: running = Stopped; break;
			case 1: running = Running; break;
			case 2: running = Paused; break;
		}
		switch (pNotificationType){
			case 0: notificationType = BuzzVibration; break;
			case 1: notificationType = Vibration; break;
			case 2: notificationType = Nothing; break;
		}
		
		switch (pNotificationWhenClosed){
			case 0: notificationWhenClosed = false; break;
			case 1: notificationWhenClosed = true; break;
		}
	}
	
	
	function getTimeString(){
		return "rootTimer";
	}
	function getSetTimeString(){
		return "rootTimer";
	}
	
	
	function getName(){
		return "rootTimer";
	}
	
	function getAbout(){
		return "rootTimer";
	}
}