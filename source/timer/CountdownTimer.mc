using Toybox.Time;
using Toybox.System;
using Toybox.Time.Gregorian;
using Toybox.Timer;
//using Toybox.Lang.Number;



class CountdownTimer extends TimerObject {
	
	var startTime;
	
	function initialize(aTime){
		TimerObject.initialize(aTime);
		stop();
	}
	
	function reset(){
		actualValue = new Time.Duration(setValue.value());
	}
	
	function start(){
    	startTime = new Time.Moment(Time.now().value());
		TimerObject.start();
	}
	
	function stop(){
		reset();
		TimerObject.stop();
	}
	
	function pause(){
		actualValue = actualValue.subtract(new Time.Duration(getSecondsFromStart()));
		TimerObject.pause();
	}
	
	function getSecondsFromStart(){
		var now = new Time.Moment(Time.now().value());
		return now.compare(startTime);
	}

	function toggle(){
		if (running == Paused && actualValue.value() == 0) { // if there is end of interval
			stop();
		} else {
			TimerObject.toggle();
		}
		
		WatchUi.requestUpdate();
	}
	
	function getTimeString(){
		if (running == Running){	
			var remainingSeconds = actualValue.value() - getSecondsFromStart();
			if (remainingSeconds <= 0){ // stop if we have done it
				return end(remainingSeconds);
			}
			return TimerObject.secondsToTimeString(remainingSeconds);
		} else if (running == Paused) {
			var remainingSeconds = actualValue.value() - getSecondsFromStart();
			if (remainingSeconds <= 0) {
				stop();
			}
			return TimerObject.secondsToTimeString(actualValue.value());
		} else if (running == Stopped) {
			return getSetTimeString();
		} else {
			return "ERROROS";
		}
	}
	
	function end(remainingSeconds){
		actualValue = new Time.Duration(0);
		TimerObject.pause();
//		stop();
		TimerObject.doNotification();
		return "00:00";
//		return TimerObject.secondsToTimeString(actualValue.value());
	}
	
	function getSetTimeString(){
		return TimerObject.secondsToTimeString(setValue.value());	
	}
	
	function convertForPersist(){
		toPersist = [];
		if (startTime instanceof Time.Moment){
			toPersist.add(startTime.value());
		} else {
			toPersist.add(startTime);
		}
		
		toPersist.add(setValue.value());
		toPersist.add(actualValue.value());
		
		TimerObject.convertForPersist();
	}

	function convertFromPersist(persistedContent){
		var persitedStartTime = persistedContent.slice(-1, null);
		startTime = new Time.Moment(persitedStartTime[0]);
		persistedContent.remove(persitedStartTime[0]);
		
		var persitedSetValue = persistedContent.slice(-1, null);
		setValue = new Time.Duration(persitedSetValue[0]);
		persistedContent.remove(persitedSetValue[0]);
		
		var persitedActualValue = persistedContent.slice(-1, null);
		actualValue = new Time.Duration(persitedActualValue[0]);
		persistedContent.remove(persitedActualValue[0]);
		
		TimerObject.convertFromPersist(persistedContent);		
	}
	
	function endWillOccurAt(){
		if (!notificationWhenClosed){
			return null;
		}
		
		if (running == Running){
			var now = new Time.Moment(Time.now().value());
			var remainingSeconds = actualValue.value() - getSecondsFromStart();
			var atMoment = now.add(new Time.Duration(remainingSeconds));
			return atMoment;
		} else {
			return null;
		}
	}
	
	
	function getName(){
		return "Countdown";
	}
	
	function getAbout(){
		return "from given duraiton";
	}
}