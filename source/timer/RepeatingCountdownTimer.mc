using Toybox.Time;
using Toybox.System;
using Toybox.Time.Gregorian;


class RepeatingCountdownTimer extends CountdownTimer {
	
	
	function initialize(aTime){
		CountdownTimer.initialize(aTime);
	}
	
	function end(remainingSeconds){
		if (remainingSeconds < 0){ // if we got so far that there is already some remaining seconds
			remainingSeconds = remainingSeconds.abs() % setValue.value();
			actualValue = new Time.Duration(remainingSeconds);	
		} 
		
		if (remainingSeconds == 0){
			CountdownTimer.reset();
			CountdownTimer.start();
			TimerObject.doNotification();
		}
		CountdownTimer.start();
		return "00:00";
	}
	
	function getName(){
		return "Repeating countdown";
	}
	
	function getAbout(){
		return "unstopable countdown";
	}
}